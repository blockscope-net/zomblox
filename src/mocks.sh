#!/bin/bash

mockPingInit() {
    PING_INTERVAL=5
}

mockPing() {
    echo "PING :: mockPing"
}


mockEpochInit() {
    I=0
    EPOCH_ID=0
}

mockEpoch() {
    if (("$I" < 3)); then
        TMP_EPOCH_ID=$I
        I=$(echo $I + 1 | bc)
    else
        echo "TURNOVER :: mockEpoch"
        TMP_EPOCH_ID=0
        I=0
    fi
}


mockStakeInit() {
    I=0
    INTERVAL=1
    STAKE_THRESHOLD=10000000000
}

mockStake() {
    if (("$I" < 2)); then
        echo "INCREASE:: mockStake"
        TMP_VOTE_STAKE=10000000000000000000000000000
        VOTE_STAKE=10000000000
        I=$(echo $I + 1 | bc)
    elif (("$I" == 2)); then
        echo "DECREASE :: mockStake"
        TMP_VOTE_STAKE=10000000000
        VOTE_STAKE=10000000000000000000000000000
        I=$(echo $I + 1 | bc)
    elif (("$I" == 3)); then
        echo "EQUAL NON ZERO :: mockStake"
        TMP_VOTE_STAKE=10000000000
        VOTE_STAKE=10000000000
        I=$(echo $I + 1 | bc)
    elif (("$I" == 4)); then
        echo "EQUAL ZERO :: mockStake"
        TMP_VOTE_STAKE=0
        VOTE_STAKE=0
        I=$(echo $I + 1 | bc)
    else
        echo "TURNOVER :: mockStake"
        TMP_VOTE_STAKE=0
        VOTE_STAKE=0
        I=0
    fi
}


mockBalanceInit() {
    I=0
    INTERVAL=5
    ALERT_TIME_THRESHOLD=15
    ALERT_REPEAT_INTERVAL=9
    BALANCE_THRESHOLD=20
}

mockBalance() {
    if (("$I" < 12)); then
        TMP_VOTE_BALANCE=$I
        VOTE_BALANCE=$I
        I=$(echo $I + 1 | bc)
    else
        echo "TURNOVER :: mockBalance"
        TMP_VOTE_BALANCE=30
        VOTE_BALANCE=20
        I=0
    fi
}


mockRpcInit() {
    I=0
    INTERVAL=5
    ALERT_TIME_THRESHOLD=15
    ALERT_REPEAT_INTERVAL=9
}

mockRpc() {
    if (("$I" < 12)); then
        ERROR_EPOCH=$I
        I=$(echo $I + 1 | bc)
    else
        echo "TURNOVER :: mockRpc"
        ERROR_EPOCH=
        I=0
    fi
}


mockSyncInit() {
    I=0
    INTERVAL=5
    ALERT_TIME_THRESHOLD=15
    ALERT_REPEAT_INTERVAL=10
}

mockSync() {
    if (("$I" < 12)); then
        TMP_VOTE_ROOT_SLOT=$I
        VOTE_ROOT_SLOT=$I
        I=$(echo $I + 1 | bc)
    else
        echo "TURNOVER :: mockSync"
        TMP_VOTE_ROOT_SLOT=8
        VOTE_ROOT_SLOT=4
        I=0
    fi
}


mockMissedSlotsInit() {
    I=0
    INTERVAL=7
    ALERT_TIME_THRESHOLD=4
    ALERT_REPEAT_INTERVAL=20
    MISSED_SLOTS_THRESHOLD=0.02
}

mockMissedSlots() {
    if (("$I" < 6)); then
        TMP_BLOCK_SLOTS_PERCENTAGE=0.97
        I=$(echo $I + 1 | bc)
    else
        echo "TURNOVER :: mockMissedSlots"
        TMP_BLOCK_SLOTS_PERCENTAGE=0.9811
        I=0
    fi
}