#!/bin/bash
source "$(dirname "$0")/mocks.sh"
source "$(dirname "$0")/config.sh"


initVars() {
    ### RESULT VARIABLES

    TMP_VOTE_BALANCE=0
    TMP_VOTE_BALANCE_LOW_WARNED_TIME=0
    TMP_IDENTITY_BALANCE=0
    TMP_IDENTITY_BALANCE_LOW_WARNED_TIME=0
    TMP_IDENTITY_BALANCE_BURN_WARNED_TIME=0
    TMP_RPC_WARNED_TIME=0
    TMP_SYNC_WARNED_TIME=0
    TMP_MISSED_SLOTS_WARNED_TIME=0
    TMP_MISSED_SLOTS_WARNED_COUNT=0
    TMP_VOTE_STAKE_IN_TOKENS=0
    TMP_VOTE_STAKE=0
    TMP_VOTE_COMMISSION=0
    TMP_VOTE_LAST=0
    TMP_VOTE_ROOT_SLOT=0

    TMP_EPOCH_ID=0
    TMP_EPOCH_TRANSACTIONS=0
    TMP_EPOCH_ABSOLUTE_SLOT=0
    TMP_EPOCH_SLOT_INDEX=0
    TMP_EPOCH_SLOT_IN_EPOCH=0
    TMP_EPOCH_PERCENT=0
    TMP_EPOCH_ALERTS_COUNT=0

    TMP_NODE_RPC_CONNECTIVITY=true
    TMP_NODE_ONLINE=true
    TMP_NODE_SHRED_VERSION=0
    TMP_NODE_VERSION=""

    TMP_BLOCK_LEADER_SLOTS=0
    TMP_BLOCK_PRODUCED_SLOTS=0
    TMP_BLOCK_SLOTS_PERCENTAGE=0

    TMP_CLUSTER_VERSION=""

    EPOCH_NEW=false

    ERROR_VOTE=""
    ERROR_EPOCH=""
    ERROR_NODE=""
    ERROR_BLOCK=""

    PING_ELAPSED_TIME=0
    ALERT_ELAPSED_TIME=0

    VOTE_BALANCE_LOW_WARNED=false
    IDENTITY_BALANCE_LOW_WARNED=false
    IDENTITY_BALANCE_BURN_WARNED=false
    SYNC_WARNED=false
    RPC_WARNED=false
    MISSED_SLOTS_WARNED=false
    VERSION_MISMATCH_WARNED=false
    NO_SLOT_PRODUCTION_WARNED=false
}


updateVars() {
	# echo "f -> updateVars"
    VOTE_BALANCE=$TMP_VOTE_BALANCE
    IDENTITY_BALANCE=$TMP_IDENTITY_BALANCE
    VOTE_STAKE_IN_TOKENS=$TMP_VOTE_STAKE_IN_TOKENS
    VOTE_STAKE=$TMP_VOTE_STAKE
    VOTE_COMMISSION=$TMP_VOTE_COMMISSION
    VOTE_LAST=$TMP_VOTE_LAST
    VOTE_ROOT_SLOT=$TMP_VOTE_ROOT_SLOT

    EPOCH_ID=$TMP_EPOCH_ID
    EPOCH_TRANSACTIONS=$TMP_EPOCH_TRANSACTIONS
    EPOCH_ABSOLUTE_SLOT=$TMP_EPOCH_ABSOLUTE_SLOT
    EPOCH_PERCENT=$TMP_EPOCH_PERCENT

    NODE_RPC_CONNECTIVITY=$TMP_NODE_RPC_CONNECTIVITY
    NODE_ONLINE=$TMP_NODE_ONLINE
    NODE_SHRED_VERSION=$TMP_NODE_SHRED_VERSION
    NODE_VERSION=$TMP_NODE_VERSION

    BLOCK_LEADER_SLOTS=$TMP_BLOCK_LEADER_SLOTS
    BLOCK_PRODUCED_SLOTS=$TMP_BLOCK_PRODUCED_SLOTS
    BLOCK_SLOTS_PERCENTAGE=$TMP_BLOCK_SLOTS_PERCENTAGE

    CLUSTER_VERSION=$TMP_CLUSTER_VERSION
}


getEpochData() {
	# echo "f -> getEpochData"
    ERROR_EPOCH=""
    RES=$(curl -s -X POST -H "Content-Type: application/json" -d '{"jsonrpc":"2.0","id":1, "method":"getEpochInfo"}' $REMOTE_RPC | jq .result)

    if [ -z "$RES" ]; then
        ERROR_EPOCH=$(curl -s -S -X POST -H "Content-Type: application/json" -d '{"jsonrpc":"2.0","id":1, "method":"getEpochInfo"}' $REMOTE_RPC 2>&1)
    else
        TMP_EPOCH_ID=$(echo $RES | jq .epoch)
        TMP_EPOCH_TRANSACTIONS=$(echo $RES | jq .transactionCount)
        TMP_EPOCH_ABSOLUTE_SLOT=$(echo $RES | jq .absoluteSlot)

        TMP_EPOCH_SLOT_INDEX=$(echo $RES | jq .slotIndex)
        TMP_EPOCH_SLOT_IN_EPOCH=$(echo $RES | jq .slotsInEpoch)
        TMP_EPOCH_PERCENT=$(echo "scale=0 ; $TMP_EPOCH_SLOT_INDEX * 100 / $TMP_EPOCH_SLOT_IN_EPOCH" | bc)
    fi
}


getVoteData() {
	# echo "f -> getVoteData"
    ERROR_VOTE=""
    RES=$(curl -s -X POST -H "Content-Type: application/json" -d '{"jsonrpc":"2.0","id":1, "method":"getVoteAccounts"}' $REMOTE_RPC | jq ".result.current[] | select(.nodePubkey==\"$IDENTITY\")")

    if [ -z "$RES" ]; then
        ERROR_VOTE=$(curl -s -S -X POST -H "Content-Type: application/json" -d '{"jsonrpc":"2.0","id":1, "method":"getVoteAccounts"}' $REMOTE_RPC 2>&1)
    else
        TMP_VOTE_BALANCE=$(curl $REMOTE_RPC -s -X POST -H "Content-Type: application/json" -d "{\"jsonrpc\":\"2.0\", \"id\":1, \"method\":\"getBalance\", \"params\":[\"$VOTE\"]}" | jq .result.value)
        TMP_VOTE_BALANCE_IN_TOKENS=$(echo "scale=2 ; $TMP_VOTE_BALANCE / 1000000000 / 1" | bc)

        TMP_IDENTITY_BALANCE=$(curl $REMOTE_RPC -s -X POST -H "Content-Type: application/json" -d "{\"jsonrpc\":\"2.0\", \"id\":1, \"method\":\"getBalance\", \"params\":[\"$IDENTITY\"]}" | jq .result.value)
        TMP_IDENTITY_BALANCE_IN_TOKENS=$(echo "scale=2 ; $TMP_IDENTITY_BALANCE / 1000000000 / 1" | bc)

        TMP_VOTE_STAKE=$(echo $RES | jq .activatedStake)
        TMP_VOTE_STAKE_IN_TOKENS=$(echo "scale=0 ; $TMP_VOTE_STAKE / 1000000000 / 1" | bc)

        TMP_VOTE_COMMISSION=$(echo $RES | jq .commission)
        TMP_VOTE_LAST=$(echo $RES | jq .lastVote)
        TMP_VOTE_ROOT_SLOT=$(echo $RES | jq .rootSlot)
    fi
}


getNodeData() {
	# echo "f -> getNodeData"
    ERROR_NODE=""
    RES=$(curl -s -X POST -H "Content-Type: application/json" -d '{"jsonrpc":"2.0","id":1, "method":"getClusterNodes"}' $REMOTE_RPC | jq ".result[] | select(.pubkey==\"$IDENTITY\")")
    RES_CLUSTER=$(curl -s -X POST -H "Content-Type: application/json" -d '{"jsonrpc":"2.0","id":1, "method":"getVersion"}' $REMOTE_RPC | jq '.result."solana-core"')

    if [ -z "$RES" ]; then
        ERROR_NODE=$(curl -s -S -X POST -H "Content-Type: application/json" -d '{"jsonrpc":"2.0","id":1, "method":"getClusterNodes"}' $REMOTE_RPC 2>&1)
        TMP_NODE_ONLINE=false
    else
        TMP_NODE_ONLINE=true
        TMP_NODE_VERSION=$(echo $RES | jq .version | tr -d '"')
        TMP_NODE_SHRED_VERSION=$(echo $RES | jq .shredVersion)

        TMP_CLUSTER_VERSION=$(echo $RES_CLUSTER | tr -d '"')
    fi
}

getBlockProductionData() {
	# echo "f -> getBlockProductionData"
    ERROR_BLOCK=""
    RES_BLOCK=$(curl $REMOTE_RPC -s -X POST -H "Content-Type: application/json" -d "{\"jsonrpc\": \"2.0\", \"id\": 1, \"method\": \"getBlockProduction\", \"params\": [{ \"identity\": \"${IDENTITY}\" }] }" | jq ".result.value.byIdentity")
    RES=$(echo $RES_BLOCK | jq ". | to_entries[].value")

    if [ -z "$RES_BLOCK" ]; then
        ERROR_BLOCK=$(curl $REMOTE_RPC -s -X POST -H "Content-Type: application/json" -d "{\"jsonrpc\": \"2.0\", \"id\": 1, \"method\": \"getBlockProduction\", \"params\": [{ \"identity\": \"${IDENTITY}\" }] }"  2>&1)
    elif [ -z "$RES" ]; then
        if [[ $NO_SLOT_PRODUCTION_WARNED == false ]]; then
            MSG="🟠🕵️ RPC ::: no leader slots production data yet! : RPC result $RES_BLOCK : 🧺"
            sendConsoleAlert "$MSG"
            NO_SLOT_PRODUCTION_WARNED=true
        fi

        TMP_BLOCK_LEADER_SLOTS=0
        TMP_BLOCK_PRODUCED_SLOTS=0
        TMP_BLOCK_SLOTS_PERCENTAGE=0
    else
        TMP_BLOCK_LEADER_SLOTS=$(echo $RES | jq .[0])
        TMP_BLOCK_PRODUCED_SLOTS=$(echo $RES | jq .[1])

        [[ -z $TMP_BLOCK_LEADER_SLOTS ]] && TMP_BLOCK_LEADER_SLOTS=0
        [[ -z $TMP_BLOCK_PRODUCED_SLOTS ]] && TMP_BLOCK_PRODUCED_SLOTS=0

        [[ $TMP_BLOCK_LEADER_SLOTS != null ]] && [[ $TMP_BLOCK_LEADER_SLOTS > 0 ]] && \
        [[ $TMP_BLOCK_PRODUCED_SLOTS != null ]] && [[ $TMP_BLOCK_PRODUCED_SLOTS > 0 ]] && \
        TMP_BLOCK_SLOTS_PERCENTAGE=$(echo "scale=4 ; $TMP_BLOCK_PRODUCED_SLOTS / $TMP_BLOCK_LEADER_SLOTS" | bc) || TMP_BLOCK_SLOTS_PERCENTAGE=0

        NO_SLOT_PRODUCTION_WARNED=false
    fi
}


sendConsoleAlert() {
    MSG="$1"
    replaceLineBreaksForBash "$MSG"

    MSG_DATE=$(date +'%Y-%m-%d %H:%M:%S')

    echo "$MSG_DATE - $BASH_MSG"
}


sendAlert() {
	## echo "f -> sendAlert"
    MSG="$1"
    TG_MSG=""
    replaceLineBreaksForTg "$MSG"

    MSG_DATE=$(date +'%Y-%m-%d %H:%M:%S')

    sendConsoleAlert "$1"
    [[ ! -z $TG_BOT_ID ]] && curl -s -X POST ${TG_URL} -d chat_id="$TG_CHAT_ID" -d text="$TG_MSG" > /dev/null 2>&1
    [[ ! -z $DISCORD_WEBHOOK ]] && curl -s -H "Accept: application/json" -H "Content-Type:application/json" -X POST --data "{\"content\": \"${MSG}\"}" ${DISCORD_WEBHOOK} > /dev/null 2>&1
}


replaceLineBreaksForTg() {
	## echo "f -> replaceLineBreaksForTg"
    TG_MSG=$(echo "$1" | sed -r 's/\\n/%0A/g')
}


replaceLineBreaksForBash() {
	## echo "f -> replaceLineBreaksForBash"
    BASH_MSG=$(echo "$1" | sed -r 's/\\n/\'$'\n''/g')
}


formatBalance() {
	# echo "f -> formatBalance"
    BALANCE=$1
    BALANCE_FORMATTED=$(echo "scale=0 ; $BALANCE / 1000000000" | bc)
    echo $BALANCE
}


# mockPingInit
sendPingAlert() {
    # mockPing
	# echo "f -> sendPingAlert"
    if (( $(echo "$PING_ELAPSED_TIME >= $PING_INTERVAL" | bc) )); then
        SLOTS_PERCENTAGE=$(echo "scale=2 ; $TMP_BLOCK_SLOTS_PERCENTAGE * 100 / 1" | bc -l)

        if [ -z "$TMP_BLOCK_PRODUCED_SLOTS" ]; then TMP_BLOCK_PRODUCED_SLOTS=0; fi
        if [ -z "$TMP_BLOCK_LEADER_SLOTS" ]; then TMP_BLOCK_LEADER_SLOTS=0; fi

        MSG="🔵🏓 PING ::: balances -> identity $TMP_IDENTITY_BALANCE_IN_TOKENS $TOKEN - vote $TMP_VOTE_BALANCE_IN_TOKENS $TOKEN : epoch $TMP_EPOCH_SLOT_INDEX/$TMP_EPOCH_SLOT_IN_EPOCH ($TMP_EPOCH_PERCENT%) : slots $TMP_BLOCK_PRODUCED_SLOTS/$TMP_BLOCK_LEADER_SLOTS ($SLOTS_PERCENTAGE%)"
        PING_ELAPSED_TIME=0
        sendAlert "$MSG"
    fi
}


# mockEpochInit
sendEpochAlert() {
    # mockEpoch
    # echo "f -> sendEpochAlert"
    if [ $TMP_EPOCH_ID != $EPOCH_ID ]; then
        MISSED_SLOTS_PERCENTAGE=$(echo "scale=2 ; $BLOCK_SLOTS_PERCENTAGE * 100 / 1" | bc -l)

        # send the previous data (last data gathered from epoch that just finished)
        MSG="🔵⚔️  EPOCH ::: epoch $EPOCH_ID : balances -> identity $TMP_IDENTITY_BALANCE_IN_TOKENS $TOKEN - vote $TMP_VOTE_BALANCE_IN_TOKENS $TOKEN : stake $TMP_VOTE_STAKE_IN_TOKENS $TOKEN : slots $BLOCK_PRODUCED_SLOTS/$BLOCK_LEADER_SLOTS ($MISSED_SLOTS_PERCENTAGE%) : alerted $TMP_EPOCH_ALERTS_COUNT times"
        sendAlert "$MSG"

        TMP_EPOCH_ALERTS_COUNT=0
        updateVars
    fi
}


# mockStakeInit
sendStakeAlert() {
    # mockStake
	# echo "f -> sendStakeAlert"
    STAKE_CHANGE=$(bc <<< "$TMP_VOTE_STAKE - $VOTE_STAKE")
    STAKE_CHANGE_IN_TOKENS=$(echo "scale=0 ; $STAKE_CHANGE / 1000000000 / 1" | bc)
    VOTE_STAKE_IN_TOKENS=$(echo "scale=0 ; $VOTE_STAKE / 1000000000 / 1" | bc)
    STAKE_THRESHOLD_IN_TOKENS=$(echo "scale=0 ; $STAKE_THRESHOLD / 1000000000 / 1" | bc)

    if [[ "${STAKE_CHANGE_IN_TOKENS#-}" > "$STAKE_THRESHOLD_IN_TOKENS" ]]; then
        (( $STAKE_CHANGE > 0 )) && EMOJI="🐋" || EMOJI="🦐"
        (( $STAKE_CHANGE > 0 )) && LEVEL_EMOJI="🔵" || LEVEL_EMOJI="🟠"
        MSG="$LEVEL_EMOJI🤿 STAKE ::: change in $STAKE_CHANGE_IN_TOKENS $TOKEN - from $VOTE_STAKE_IN_TOKENS to $TMP_VOTE_STAKE_IN_TOKENS : $EMOJI"
        sendAlert "$MSG"
    fi
}


# mockBalanceInit
sendBalanceAlert() {
    # mockBalance
	# echo "f -> sendBalanceAlert"

    if (( $(echo "$TMP_VOTE_BALANCE < $BALANCE_THRESHOLD" | bc -l) )); then
        if (("$TMP_VOTE_BALANCE_LOW_WARNED_TIME" >= "$ALERT_TIME_THRESHOLD")) && [[ $VOTE_BALANCE_LOW_WARNED == false ]]; then
            MSG="🟠🍖 BALANCE ::: low vote balance! $TMP_VOTE_BALANCE_IN_TOKENS $TOKEN : 🦴"
            VOTE_BALANCE_LOW_WARNED=true
            TMP_EPOCH_ALERTS_COUNT=$(bc <<< "$TMP_EPOCH_ALERTS_COUNT + 1")
            sendAlert "$MSG"
        fi

        TMP_VOTE_BALANCE_LOW_WARNED_TIME=$(bc <<< "$TMP_VOTE_BALANCE_LOW_WARNED_TIME + $INTERVAL")
    elif (( $(echo "$TMP_VOTE_BALANCE > $BALANCE_THRESHOLD" | bc -l) )); then
        if [[ $VOTE_BALANCE_LOW_WARNED == true ]] || (( $TMP_VOTE_BALANCE_LOW_WARNED_TIME > $ALERT_TIME_THRESHOLD )); then
            MSG="🟢🍖 BALANCE ::: enough vote balance $TMP_VOTE_BALANCE_IN_TOKENS $TOKEN : 🥩"
            VOTE_BALANCE_LOW_WARNED=false
            sendAlert "$MSG"
        fi

        TMP_VOTE_BALANCE_LOW_WARNED_TIME=0
    fi

    if (( $(echo "$TMP_IDENTITY_BALANCE < $BALANCE_THRESHOLD" | bc -l) )); then
        if (("$TMP_IDENTITY_BALANCE_LOW_WARNED_TIME" >= "$ALERT_TIME_THRESHOLD")) && [[ $IDENTITY_BALANCE_LOW_WARNED == false ]]; then
            MSG="🟠🍖 BALANCE ::: low identity balance! $TMP_IDENTITY_BALANCE_IN_TOKENS $TOKEN : 🦴"
            IDENTITY_BALANCE_LOW_WARNED=true
            TMP_EPOCH_ALERTS_COUNT=$(bc <<< "$TMP_EPOCH_ALERTS_COUNT + 1")
            sendAlert "$MSG"
        fi

        TMP_IDENTITY_BALANCE_LOW_WARNED_TIME=$(bc <<< "$TMP_IDENTITY_BALANCE_LOW_WARNED_TIME + $INTERVAL")
    elif (( $(echo "$TMP_IDENTITY_BALANCE > $BALANCE_THRESHOLD" | bc -l) )); then
        if [[ $IDENTITY_BALANCE_LOW_WARNED == true ]] || (( $TMP_IDENTITY_BALANCE_LOW_WARNED_TIME > $ALERT_TIME_THRESHOLD )); then
            MSG="🟢🍖 BALANCE ::: enough identity balance $TMP_IDENTITY_BALANCE_IN_TOKENS $TOKEN : 🥩"
            IDENTITY_BALANCE_LOW_WARNED=false
            sendAlert "$MSG"
        fi

        TMP_IDENTITY_BALANCE_LOW_WARNED_TIME=0
    fi

#    if (( $(echo "$TMP_IDENTITY_BALANCE == $IDENTITY_BALANCE" | bc -l) )); then
#        if (("$TMP_IDENTITY_BALANCE_BURN_WARNED_TIME" >= "$ALERT_TIME_THRESHOLD")) && [[ $IDENTITY_BALANCE_BURN_WARNED == false ]]; then
#            MSG="🔴🍖 BALANCE ::: not burning tokens!!! : vote balance $IDENTITY_BALANCE == $TMP_IDENTITY_BALANCE (lamports) : 💦"
#            IDENTITY_BALANCE_BURN_WARNED=true
#            TMP_EPOCH_ALERTS_COUNT=$(bc <<< "$TMP_EPOCH_ALERTS_COUNT + 1")
#            sendAlert "$MSG"
#        fi
#
#        TMP_IDENTITY_BALANCE_BURN_WARNED_TIME=$(bc <<< "$TMP_IDENTITY_BALANCE_BURN_WARNED_TIME + $INTERVAL")
#    elif (( $(echo "$TMP_IDENTITY_BALANCE != $IDENTITY_BALANCE" | bc -l) )); then
#        if [[ $IDENTITY_BALANCE_BURN_WARNED == true ]] || (( $TMP_IDENTITY_BALANCE_BURN_WARNED_TIME > $ALERT_TIME_THRESHOLD )); then
#            MSG="🟢🍖 BALANCE ::: burning tokens back again : vote balance $IDENTITY_BALANCE -> $TMP_IDENTITY_BALANCE (lamports) : 🔥"
#            IDENTITY_BALANCE_BURN_WARNED=false
#            sendAlert "$MSG"
#        fi
#
#        TMP_IDENTITY_BALANCE_BURN_WARNED_TIME=0
#    fi
}


# mockRpcInit
sendRpcAlert() {
    # mockRpc
	## echo "f -> sendRpcAlert"
    if [[ ! -z "$ERROR_EPOCH" ]] || [[ ! -z "$ERROR_VOTE" ]] || [[ ! -z "$ERROR_NODE" ]] || [[ ! -z "$ERROR_BLOCK" ]]; then
        if (("$TMP_RPC_WARNED_TIME" >= "$ALERT_TIME_THRESHOLD")) && [[ $RPC_WARNED == false ]]; then
            MSG="🟠🕵️ RPC ::: rpc offline! : 🔧"
            RPC_WARNED=true
            TMP_EPOCH_ALERTS_COUNT=$(bc <<< "$TMP_EPOCH_ALERTS_COUNT + 1")
            sendAlert "$MSG"
        fi

        TMP_RPC_WARNED_TIME=$(bc <<< "$TMP_RPC_WARNED_TIME + $INTERVAL")
    else
        if [[ $RPC_WARNED == true ]] || (( $TMP_RPC_WARNED_TIME > $ALERT_TIME_THRESHOLD )); then
            MSG="🟢🕵️ RPC ::: rpc online back again : 🔎"
            RPC_WARNED=false
            sendAlert "$MSG"
        fi

        TMP_RPC_WARNED_TIME=0
    fi
}


# mockSyncInit
sendSyncAlert() {
    # mockSync
	## echo "f -> sendSyncAlert"
    if (( $TMP_VOTE_ROOT_SLOT == $VOTE_ROOT_SLOT )); then
        if (("$TMP_SYNC_WARNED_TIME" >= "$ALERT_TIME_THRESHOLD")) && [[ $SYNC_WARNED == false ]]; then
            MSG="🔴📺 NODE ::: not syncing!!! : stalled at slot $TMP_VOTE_ROOT_SLOT : ⚰️ "
            SYNC_WARNED=true
            TMP_EPOCH_ALERTS_COUNT=$(bc <<< "$TMP_EPOCH_ALERTS_COUNT + 1")
            sendAlert "$MSG"
        fi

        TMP_SYNC_WARNED_TIME=$(bc <<< "$TMP_SYNC_WARNED_TIME + $INTERVAL")
    elif (( $TMP_VOTE_ROOT_SLOT > $VOTE_ROOT_SLOT )); then
        if [[ $SYNC_WARNED == true ]] || (( $TMP_SYNC_WARNED_TIME > $ALERT_TIME_THRESHOLD )); then
            MSG="🟢📺 NODE ::: syncing back again : slots advancing $VOTE_ROOT_SLOT -> $TMP_VOTE_ROOT_SLOT : 🕺"
            SYNC_WARNED=false
            sendAlert "$MSG"
        fi

        TMP_SYNC_WARNED_TIME=0
    fi
}


# mockMissedSlotsInit
sendMissedSlotsAlert() {
    # mockMissedSlots
	# echo "f -> sendMissedSlotsAlert"
    (( $(echo "$TMP_BLOCK_SLOTS_PERCENTAGE > 0" | bc -l) )) && MISSED_SLOTS_FRACTION=$(echo "scale=2 ; 1 - $TMP_BLOCK_SLOTS_PERCENTAGE" | bc) || MISSED_SLOTS_FRACTION=0
    MISSED_SLOTS_PERCENTAGE=$(echo "scale=2 ; $MISSED_SLOTS_FRACTION * 100 / 1" | bc)

    if (( $(echo "$MISSED_SLOTS_FRACTION >= $MISSED_SLOTS_THRESHOLD" | bc -l) )) &&  (( $(echo "$TMP_BLOCK_SLOTS_PERCENTAGE > $BLOCK_SLOTS_PERCENTAGE" | bc -l) || $(echo "$TMP_MISSED_SLOTS_WARNED_COUNT == 0" | bc -l) )); then
        if (("$TMP_MISSED_SLOTS_WARNED_TIME" >= "$ALERT_TIME_THRESHOLD")) && (("$TMP_MISSED_SLOTS_WARNED_COUNT" < "$MISSED_SLOTS_MAX_WARN")) && [[ $MISSED_SLOTS_WARNED == false ]]; then
            MSG="🟠📺 NODE ::: missing too many slots! : missed $MISSED_SLOTS_PERCENTAGE% ($TMP_BLOCK_PRODUCED_SLOTS/$TMP_BLOCK_LEADER_SLOTS) : 🚜"
            MISSED_SLOTS_WARNED=true
            TMP_MISSED_SLOTS_WARNED_COUNT=$(bc <<< "$TMP_MISSED_SLOTS_WARNED_COUNT + 1")
            TMP_EPOCH_ALERTS_COUNT=$(bc <<< "$TMP_EPOCH_ALERTS_COUNT + 1")
            sendAlert "$MSG"
        fi

        TMP_MISSED_SLOTS_WARNED_TIME=$(bc <<< "$TMP_MISSED_SLOTS_WARNED_TIME + $INTERVAL")
    elif (( $(echo "$MISSED_SLOTS_FRACTION < $MISSED_SLOTS_THRESHOLD" | bc -l) )); then
        if [[ $MISSED_SLOTS_WARNED == true ]] || (( $TMP_MISSED_SLOTS_WARNED_TIME > $ALERT_TIME_THRESHOLD )); then
            if [ -z "$TMP_BLOCK_PRODUCED_SLOTS" ]; then TMP_BLOCK_PRODUCED_SLOTS=0; fi
            if [ -z "$TMP_BLOCK_LEADER_SLOTS" ]; then TMP_BLOCK_LEADER_SLOTS=0; fi

            MSG="🟢📺 NODE ::: missing slots back to normal : missed $MISSED_SLOTS_PERCENTAGE% ($TMP_BLOCK_PRODUCED_SLOTS/$TMP_BLOCK_LEADER_SLOTS) : ✈️ "
            MISSED_SLOTS_WARNED=false
            TMP_MISSED_SLOTS_WARNED_COUNT=0
            sendAlert "$MSG"
        fi

        TMP_MISSED_SLOTS_WARNED_TIME=0
    fi
}


sendVersionAlert() {
    sendVersionNodeAlert
    sendVersionClusterAlert
}


sendVersionNodeAlert() {
    if [[ "$TMP_NODE_VERSION" != "$NODE_VERSION" ]] && [[ ! -z "$TMP_NODE_VERSION" ]] && [[ ! -z "$NODE_VERSION" ]]; then
        MSG="🔵📺 NODE ::: new node version v$NODE_VERSION -> v$TMP_NODE_VERSION : remote RPC version v$TMP_CLUSTER_VERSION : 📦"
        sendAlert "$MSG"
        sendVersionMismatchAlert
    fi
}


sendVersionClusterAlert() {
    if [[ "$TMP_CLUSTER_VERSION" != "$CLUSTER_VERSION" ]] && [[ ! -z "$TMP_CLUSTER_VERSION" ]] && [[ ! -z "$CLUSTER_VERSION" ]]; then
        MSG="🔵📺 NODE ::: node version v$TMP_NODE_VERSION : new remote RPC version v$CLUSTER_VERSION -> v$TMP_CLUSTER_VERSION : 🔗"
        sendAlert "$MSG"
        sendVersionMismatchAlert
    fi
}


sendVersionMismatchAlert() {
    if [[ "$TMP_NODE_VERSION" != "$TMP_CLUSTER_VERSION" ]]; then
        if [[ ! -z "$TMP_NODE_VERSION" ]] && [[ ! -z "$TMP_CLUSTER_VERSION" ]]; then
            MSG="🟠📺 NODE ::: version mismatch between node and remote RPC (v$TMP_NODE_VERSION <> v$TMP_CLUSTER_VERSION) : 🔀"
            VERSION_MISMATCH_WARNED=true
            sendAlert "$MSG"
        fi
    elif [[ $VERSION_MISMATCH_WARNED == true ]]; then
        MSG="🟢📺 NODE ::: version up to date between node and remote RPC (v$TMP_NODE_VERSION <> v$TMP_CLUSTER_VERSION) : 🤖"
        VERSION_MISMATCH_WARNED=false
        sendAlert "$MSG"
    fi
}


resetWarnedAlerts() {
    if (( $(echo "$ALERT_ELAPSED_TIME >= $ALERT_REPEAT_INTERVAL" | bc) )); then
        VOTE_BALANCE_LOW_WARNED=false
        IDENTITY_BALANCE_LOW_WARNED=false
        IDENTITY_BALANCE_BURN_WARNED=false
        RPC_WARNED=false
        SYNC_WARNED=false
        MISSED_SLOTS_WARNED=false
        VERSION_MISMATCH_WARNED=false
        NO_SLOT_PRODUCTION_WARNED=false

        ALERT_ELAPSED_TIME=0
    fi
}


init() {
	# echo "f -> init"
    sendAlert "🧟🧟 Starting Zomblox! 🧟🧟"
    sendAlert "👷 NOTICE \n  * this is ${VERSION} version (use it on your own behalf 🤕)\n  * by Blockscope ⌨️ \n  * help us feed the zombie by \n    - spreading the infection  🧟\n    - submiting bugs/PR's to GitLab repo  💾\n    - delegating with us $VALIDATOR_ADDRESS  🥩\n    - donating $TOKEN to $ADDRESS  🏂\n"

    initVars
    updateVars

    getEpochData
    getVoteData
    getNodeData
    getBlockProductionData
    updateVars

    sendVersionMismatchAlert

    sleep $INTERVAL
}


sendWarningAlerts() {
	# echo "f -> sendWarningAlerts"
    sendBalanceAlert
    sendRpcAlert
    sendSyncAlert
    sendMissedSlotsAlert
    sendVersionAlert
}


### MAIN LOOP
init

while :
do
    getEpochData
    getVoteData
    getNodeData
    getBlockProductionData

    resetWarnedAlerts

    sendEpochAlert
    sendStakeAlert
    sendPingAlert
    sendWarningAlerts

    updateVars

    sleep $INTERVAL

    PING_ELAPSED_TIME=$(bc <<< "$PING_ELAPSED_TIME + $INTERVAL")
    ALERT_ELAPSED_TIME=$(bc <<< "$ALERT_ELAPSED_TIME + $INTERVAL")
	# echo "..."
done
