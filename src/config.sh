#!/bin/bash

### CONFIG VARIABLES (MUST BE SET WITH YOUR NODE IDENTITY, RPC ADDRESS, AND DISCORD/TELEGRAM DATA) ###
IDENTITY="ESqbklmScrAtch3WrRXWtmm11xSM99yUTjtJDXekxCTTF"    # REPLACE with your node identity
VOTE="ESqbklmScrAtch3WrRXWtmm11xSM99yUTjtJDXekxCTTF"        # REPLACE with your node identity

# REMOTE_RPC="https://api.testnet.solana.com"               # Solana testnet (Solana public RPC's - https://docs.solana.com/cluster/rpc-endpoints)
# REMOTE_RPC="https://api.mainnet-beta.solana.com"          # Solana mainnet beta
REMOTE_RPC="http://localhost:8899"                          # localhost RPC (prefered RPC for more stable response and not overload public RPC!!)

DISCORD_WEBHOOK="https://discord.com/api/webhooks/936203650299691/YC2QT9IFjVfcr9I-zfujRCp_B-Y3ndY2A4TDT9IFGyZ_aC2QsY4sfLHJL-Y3ndGyZ_"   # REPLACE with your discord webhook url
TG_BOT_ID="29503478392:BBE246ASVd_oIlV4IPasdGSJelTSFir9nwOIfkM"     # REPLACE with your Telegram bot id
TG_CHAT_ID="2305783948"                                             # REPLACE with your Telegram bot chat id
### CONFIG VARIABLES END ###

### CUSTOMIZATION VARIABLES (MODIFY TO YOUR NEEDS) ###
INTERVAL=10                             # Time interval in seconds to query the RPC and detect alerts / events
PING_INTERVAL=10800                     # Time interval in seconds for sending a Ping message
ALERT_REPEAT_INTERVAL=600               # Time interval in seconds for re-sending important alerts if the issue hasn't been reverted
ALERT_TIME_THRESHOLD=120                # Time threshold in seconds for an alert to be sent (ignore intermitent issues)
STAKE_THRESHOLD=1000000000              # Amount threshold for a stake change to considered as an alert (ignore small changes) (Example: 1000000000 equals 1 SOL)
BALANCE_THRESHOLD=1000000000            # Vote account minimum balance threshold for an alert to be sent (Example: 1000000000 equals 1 SOL)
MISSED_SLOTS_THRESHOLD=0.3              # Missed slots fraction threshold for sending alerts (Example: 0.15 equals 15%, 0.05 equals 5%)
MISSED_SLOTS_MAX_WARN=10                # Missed slots max warnings to be sent until slots back to normal
### CUSTOMIZATION VARIABLES END ###


### INTERNAL VARIABLES (DO NOT CHANGE!!!)
TOKEN="SOL"

TG_URL="https://api.telegram.org/bot${TG_BOT_ID}/sendMessage"

# Credtis
VERSION="v0.0.2-beta"
ADDRESS="BuFaoSo9BXukurTF5hrKt3W7TbEC2WL72E6zRGWRDe4d"
VALIDATOR_ADDRESS="GzdpwmsqTaEK2yk2s1xdmzXEfH3P1UnKjZR7u7nSNXbi"
### INTERNAL VARIABLES END ###