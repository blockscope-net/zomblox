# 🧟 Zomblox

A Solana monitoring tool to alert you from your validator becoming a zombi.

![Zomblox Solana Monitoring](software-ad-zomblox.png)

## 🗒 Description

Zomblox is a monitoring and alerting tool for Solana validator nodes, it aims to be simple to install and use, and useful in features and information provided.

Zomblox displays your node's info in a friendly way, via your favourite communication channels: Telegram, Discord and of course the shell.

## 💾 Installation
Zomblox is a shell script, so to make it work just install dependencies, clone the repo and execute it.

### **Install dependencies**
```sh
sudo apt install bc jq curl sed git -y
```

### **Clone the repository**
```sh
cd ~
git clone https://gitlab.com/blockscope-net/zomblox.git
cd zomblox
git checkout v0.0.2-beta
```

### **Make the script executable**
```sh
cd ./src/
chmod +x zomblox.sh
```

### **Make sure you have full RPC API enabled (only if using localhost RPC)**
Add the following flag `--full-rpc-api` to your node startup command, if not present already.

```sh
#!/bin/bash
exec solana-validator \
    --identity ./validator-keypair.json \
    ...
    --rpc-bind-address 127.0.0.1 \
    --rpc-port 8899 \
    --full-rpc-api                # this flag should be used
```

## 🏃 Run it
To run Zomblox you can just run the shell script or create a service unit to get it always running (check next section).

```sh
./zomblox.sh
```

## 😈 Run with a Service
As a monitoring tool it's better to handle its execution with a Linux service, so it starts on boot/reboots and restarts in the case it exits the execution.

### **Create service file**

```sh
SOLANA_PATH=$(dirname "$(which solana)")

echo "[Unit]
Description=Zomblox Service

[Service]
User="$USER"
Group="$USER"
Environment=PATH=$PATH:$SOLANA_PATH
ExecStart="$PWD/zomblox.sh"
Restart=on-failure
RestartSec=5s

[Install]
WantedBy=multi-user.target " > zomblox.service
```

### **Move service file to services location**
```sh
sudo mv ./zomblox.service /etc/systemd/system/zomblox.service
```

### **Enable the service**
```sh
sudo systemctl daemon-reload
sudo systemctl enable zomblox
```

### **Run the service**
```sh
sudo systemctl start zomblox
```

### **Check logs / output**
```sh
sudo journalctl -u zomblox -fo cat
```

### **Stop the service**
```sh
sudo systemctl stop zomblox
```

### **Check service status**
```sh
sudo systemctl status zomblox
```

## 🏓 Usage
As a monitoring/alerting tool the main thing in Zomblox is sending you alerts of important events on your validator node. Here is a description on how the info messages are structured and the meaning of its parts.

### **Messages**
A Zomblox message consists of four parts in this order:

* Category: an emoji indicating the type of the message
* Section: an emoji and name indicating to what section the alert belongs to
* Content: a meaningful message depicting the event and related info
* Id: an emoji assigned to each message

### **Categories**
Are represented with a coloured ball emoji representing a type of message.

* `🔵 INFO: messages that just communicate interesting info`
* `🟠 WARN: messages that alert from something important that could indicate some issue`
* `🔴 ERROR: messages that alert from a problem that is critical`
* `🟢 OK: messages that communicate that a WARN or an ERROR has been reverted`

### **Sections**
Are represented with an emoji related to the part of the system that the alert regards to.

* `📺 Node: messages regarding your node or the network cluster (not syncing, missing slots, version events)`
* `🕵️ RPC: messages regarding the RPC communication (offline rpc, no slots production data, etc.)`
* `🍖 Balance: messages regarding the balance of the voting and identiy accounts (low balance, not burning tokens)`
* `🏓 Ping: messages sent periodically, with some generic info (vote and identity balances, epoch progress, slots produced)`
* `⚔️ Epoch: messages sent when a new epoch starts, with some generic info (epoch id, vote and identity balances, stake, slots produced)`
* `🤿 Stake: messages regarding changes in validator stake`

### **Id**
Each message is assigned a unique emoji for visual purposes, so once you know the messages you can identify which one you're reading by just looking at the emoji.

## 👨‍🔧 Configuration

### **Config**
There are some variables at the begining of the config file (`config.sh`) that you need to set in order for Zomblox to work
```sh
IDENTITY="SET_THE_IDENTITY_OF_YOUR_NODE_HERE"
REMOTE_RPC="http://localhost:8899"                # use your localhost RPC or another one
```

*Solana official RPCs https://docs.solana.com/cluster/rpc-endpoints*

### **Customize**
Below the configuration variables you can find some others that you can tweak in order to adjust Zomblox behaviour to your needs. Time intervals are specified in seconds, token amounts in lamports (1000000000 lamports == 1 SOL token).

```sh
INTERVAL=10                         # Time interval to query the RPC and detect alerts / events
PING_INTERVAL=10800                 # Time interval for sending a Ping message
ALERT_REPEAT_INTERVAL=300           # Time interval for re-sending important alerts if the issue hasn't been reverted
ALERT_TIME_THRESHOLD=60             # Time threshold for an alert to be sent (ignore intermitent issues)
STAKE_THRESHOLD=1000000000          # Amount threshold for a stake change to considered as an alert (ignore small changes)
BALANCE_THRESHOLD=1000000000        # Vote and identity accounts minimum balance threshold for an alert to be sent
MISSED_SLOTS_THRESHOLD=0.15         # Missed slots fraction threshold for sending alerts (Example: 0.15 equals 15%)
MISSED_SLOTS_MAX_WARN=10            # Missed slots max warnings to be sent until slots back to normal
```

### **Telegram and Discord**
Besides showing logs in the shell terminal Zomblox can send the alerts to your Discord servers and Telegram bots, for that you'll need to set up your Discord integration or Telegram bot creation. You can find tons of resources for doing that.

For Discord just add your webhook url.
```sh
DISCORD_WEBHOOK="SET_YOUR_DICORD_WEBHOOK_URL"
```

For Telegram set your bot id and the chat id of your telegram program with the bot.
```sh
TG_BOT_ID="SET_YOUR_BOT_ID"
TG_CHAT_ID="SET_YOUR_CHAT_ID"
```

## 👀 Visuals

Telegram screenshot

![image-1.png](./image-1.png)

Discord screenshot

![image.png](./image.png)

Console screenshot

![image-2.png](./image-2.png)


## 💡 Troubleshooting

- If you get a `solana: command not found`, the problem is that your solana binary is not in the path, so add it to your bash path. Alternatively you can modify the `COMMAND` variable with the full path of your solana binary. For example:

```sh
# get the binary path -> output: /home/myuser/.local/share/solana/install/active_release/bin/solana
which solana

# edit COMMAND variable in config.sh file
COMMAND="/home/myuser/.local/share/solana/install/active_release/bin/solana"
```

- If you get RPC errors like `Method not found` or others, it may be caused by that your node has not been started the `--full-rpc-api` flag, so add it as explained in the installation section. You can check it by doing the following curl request:
```bash
REMOTE_RPC="http://localhost:8899"
curl -s -X POST -H "Content-Type: application/json" -d '{"jsonrpc":"2.0","id":1, "method":"getClusterNodes"}' $REMOTE_RPC
```
if you get the following response `{"jsonrpc":"2.0","error":{"code":-32601,"message":"Method not found"},"id":1}`, you should use the full rpc api flag


## 🛣 Roadmap

Release Alpha (1st July)
- MVP Delevopment
- Advanced features
- Configuration development
- Telegram / Discord integration
- Final testing (testnet and mainnet)
- Improvements

Release Beta (15st August)
- UX, formatting & documentation
- System service
- Publish release
- Communication (Discord, Telegram channels)
- Feedback
- Bug fixes & improvements

## 🔢 Versions

```
v0.0.1-alpha - 01/07/2022
v0.0.1-beta  - 15/08/2022
v0.0.2-beta  - 18/08/2022
```

## 📬 Support

Contact us throught Gitlab or via email at blockscope@protonmail.com

## 🛠 Contributing

Feel free to contribute new features by creating a Pull Request in this repository. Get in contact with us for any doubt on how to develop before submitting the PR to avoid wasting development time.

## ✅ Project status

Alive! Zombies keep updating the project with bug fixes and improvements, don't hesitate to submit an issue to the repository.

## 🧟 Authors and acknowledgment

Developed by zombies at Blockscope for Solana validators community with the help from the Solana Foundation.

If you use and like Zomblox, feel free to:

- delegate SOL with us (once our Blockscope validator is onboarded and live on mainnet)
- donate some SOL to buy some coffee ☕ and food 🦶 for the zombies 🧟

```
Delegations: GzdpwmsqTaEK2yk2s1xdmzXEfH3P1UnKjZR7u7nSNXbi
Donations: BuFaoSo9BXukurTF5hrKt3W7TbEC2WL72E6zRGWRDe4d
```

Thanks for your support!

## 📝 License

This project is subject to the GNU General Public License v3 (GPL-3)
[GNU GPL License](<https://tldrlegal.com/license/gnu-general-public-license-v3-(gpl-3)>).
