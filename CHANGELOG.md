# ZOMBLOX CHANGELOG


# Version 0.0.2-beta (2022-08-18)
## Display only in console and only once alert message for when there's no leader slots production data yet
🟠🕵️ RPC ::: no leader slots production data yet! : RPC result $RES_BLOCK : 🧺

## Set slot production to 0 when there's no data, so first ping alerts in epoch don't show previous epoch's data

## Remove COMMAND deprecated variable

## Add zomblox VERSION variable displayed in startup message


# Version 0.0.1-beta (2022-08-16)
## Added alert message for when there's no leader slots production data yet
🟠🕵️ RPC ::: no leader slots production data yet! : RPC result $RES_BLOCK : 🧺


## Fixed issue sending alerts of 0 SOL stake change with small stake amounts
🟠🤿 STAKE ::: change in 0 SOL - from 30824 to 30824 : 🦐


## Added both accounts (vote and identity) balance in ping alerts (suggested by validator operator)
🔵🏓 PING ::: balances -> identity 57.30 SOL - vote 2431.93 SOL : epoch 133789/432000 (30%) : slots 32/36 (88.88%)


## Added both accounts (vote and identity) balance in new epoch alerts
🔵⚔️  EPOCH ::: epoch 349 : balances -> identity 57.66 SOL - vote 2431.93 SOL : stake 30774 SOL : slots 78/92 (84.78%)


## Added low balance alerts for vote account also
🟠🍖 BALANCE ::: low vote balance! $VOTE_BALANCE : 🦴
🟢🍖 BALANCE ::: enough vote balance $VOTE_BALANCE : 🥩
🟠🍖 BALANCE ::: low identity balance! $IDENTITY_BALANCE : 🦴
🟢🍖 BALANCE ::: enough identity balance $IDENTITY_BALANCE : 🥩


## Added count of alerts triggered during epoch in new apoch alerts
🔵⚔️  EPOCH ::: epoch 349 : balances -> identity 57.66 SOL - vote 2431.93 SOL : stake 30774 SOL : slots 78/92 (84.78%) : alerted 23 times


## Added configurable limit of missed slots alerts to avoid sending too many alerts if node is not performing well
MISSED_SLOTS_MAX_WARN=10                # Missed slots max warnings to be sent until slots back to normal


## Removed the dependency from 'solana' command to retrieve the cluster version, all info requested directly from the RPC
solana cluster-version -u $REMOTE_RPC
### replaced by
curl -s -X POST -H "Content-Type: application/json" -d '{"jsonrpc":"2.0","id":1, "method":"getVersion"}' $REMOTE_RPC | jq '.result."solana-core"'


## Fixed issue with block production data RPC response data structure
echo $RES_BLOCK | jq ". | to_entries[].value"


## Added date and time for console alert logs
2022-08-16 11:08:58 - 🔵🏓 PING ::: balances -> identity 57.24 SOL - vote 2431.93 SOL : epoch 156044/432000 (36%) : slots 39/44 (88.63%)

2022-08-16 11:09:09 - 🔵🏓 PING ::: balances -> identity 57.24 SOL - vote 2431.93 SOL : epoch 156063/432000 (36%) : slots 39/44 (88.63%)